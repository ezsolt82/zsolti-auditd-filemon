# BitNinja malware file monitoring inotifywait replacement with auditd

This repo conatins the proof of concept scripts to use auditd
for file monitoring instead of inotifywait.



Installation
-------------

1. Check out the repo in the /root directory

```bash
cd /root
git clone https://ezsolt82@bitbucket.org/ezsolt82/zsolti-auditd-filemon.git
```
	
2. Prepare BitNinja and install the audit package (Run this step only once!)

```bash
./prepare.sh
```

3. Start the auditing and audit log processing
	This will flush any previous audit rules, and insert a rule to watch the /home dir
	and then start the run.php script in a screen session to process auditd logs
	This will generate inotify compatible logs for BitNinja malware detection from the
	audit logs. You can stop it by executing the stop.sh script

```bash
./start.sh
```
	
4. Go to admin.bitninja.io and enable the Malware Detection module

	
6. To stop the audit logging run:
	
```bash
./stop.sh
```	
	
This is just a Proof of Concept prototype! 	

