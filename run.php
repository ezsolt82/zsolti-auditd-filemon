#!/opt/bitninja-dojo/run/bin/bitninja-dojo -c /opt/bitninja/etc/php.ini
<?php

echo "Processing audit logs..\n";

$cache=[];

for (;;){
    $out=[];
    
    // Dirty method to handle timezone difference!!
    $cmd = "ausearch -k bn_filemon --start ".date("H:i:s", time()-10+60*60)." 2>&1";
    exec($cmd, $out);
    echo $cmd;

    if ($out[0] == "<no matches>") {
	echo ".";
    } else {
	//echo "\nNew records detected.\n";
	//var_dump($out);
	
	$record=[];
	$first_record_processed = false;
	foreach ($out as $line){
	    // var_dump($line);
	    if ($line == "----") {
		if (isset($record["file"]) && isset($record["dir"])) {
	    	    process_record($record, $cache, $first_record_processed);
		}
		$record=[];
		continue;
	    }
	    
	    if (strpos($line, "time->") !== false){
		$record["time"] = substr($line, 6);
		continue;
	    }
	    
	    $key_value_pairs = explode(" ", $line);
	    $keys = [];
	    foreach ($key_value_pairs as $key_value_pair){
		[$key, $value] = explode("=", $key_value_pair);
		$keys[$key] = $value;
	    }
	    
	    if (strpos($line, "objtype=UNKNOWN") !== false) continue;
	    
	    if ((strpos($line, "type=PATH") !== false) && (strpos($line, "item=0") !== false)){
		$record["dir"] = trim($keys["name"], '"');
		$record["id"] = trim($keys["msg"]);
	    }

	    if ((strpos($line, "type=PATH") !== false) && (strpos($line, "item=1") !== false)){
		$record["file"] = trim($keys["name"], '"');
	    }
	    
	    // Skip other record types.
	    //if (strpos($line, "type=PATH") === false) continue;
        }
        
        if (isset($record["file"]) && isset($record["dir"])) {
	    process_record($record, $cache, $first_record_processed);
	}
    }
    sleep(1);
}


function process_record($record, &$cache, $first_record_processed){
    // This is dirty I know. Need to refactor for production!!

    if ($record["file"][0] == "/"){
	$path = $record["file"];
    } else {
	$path = $record["dir"]."/".$record["file"];
    }


    // Dirty and inefficient check to avoid duplicate report of the same file.
    // For prod need to refactor to filter based on the event time.

    $line = $path." MODIFY ".date("Y-m-d H:i:s");

    if (! $first_record_processed){
	// cleanup cache
	$released=0;
	foreach ($cache as $id=>$time){
	    if ($time < $cache[$record["id"]]){
		unset($cache[$id]);
		$released++;
	    }
	}
	
	if ($released !== 0){
	    // Workaround about memory leak
	    gc_collect_cycles();
	    $memory_mb = round(memory_get_usage(true)/1024/1024);
	    if ($memory_mb > 10){
		exit(0);
	    }
	    $line = date("Y-m-d H:i:s")." Cache cleaned (".$released.") objects. Used memory: ".$memory_mb." MB cache size: ".count($cache)."\n";
	    echo $line;
	    file_put_contents('memory.log', $line, FILE_APPEND);
	}
	$first_record_processed=true;
    }

    if (isset($cache[$record["id"]]) ){
	return;
    }

    // Do not process directory events.
    if (is_dir($path)){
	return;
    }
    
    $cache[$record["id"]]= time();
    
    file_put_contents("/var/log/bitninja/inotify/inotify.log", $line."\n", FILE_APPEND);

    echo $line."\n";
}