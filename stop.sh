#!/bin/bash

screen -X -S bitninja_auditd quit

echo "The background autit logger process has stopped."

echo "Stopping auditd based file monitoring."
echo "Deleting all audit rules."

auditctl -D
