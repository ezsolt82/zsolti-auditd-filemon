#!/bin/bash

echo -e "\n\n"
echo -e "Preparing BitNinja for the auditd file monitoring PoC functionality"
echo -e "You need to run this script only once"
echo -e "\n\n"
echo "Setting dummy config for BitNinja malware detection inotify.."

echo "file_path[] = '/opt/bitninja/etc/devel'" >> /etc/bitninja/MalwareDetection/config.ini
echo "file_path[] = '/opt/bitninja/etc'" >> /etc/bitninja/MalwareDetection/config.ini

echo "..done"

echo -e "\n\nKilling the inotifywait process if there's any."

pkill inotifywait

echo "Installing the audit package"

yum -y install audit screen

service auditd start

echo -e "\n\nNow you can enable the MalwareDetection module, it won't start inotifywait on the home directories."
echo -e "Then you can run the start.sh to start up the auditd based file monitoring."

