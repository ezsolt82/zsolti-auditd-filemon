#!/bin/bash

echo -e "\n\n"
echo -e "Enabling the proof of concept auditd file monitoring functionality."
echo -e "You can disable this by running the stop.sh script."
echo -e "\n\n"

echo "Deleting all audit rules"
auditctl -D

echo "Adding audit rule to watch /home, /home2, /home3"
auditctl -w /home/ -p wa -k bn_filemon
auditctl -w /home2/ -p wa -k bn_filemon
auditctl -w /home3/ -p wa -k bn_filemon

echo "Checking for screen installation"

if ! rpm -qa | grep -qw screen; then
    echo "Installing screen.."
    yum -y install screen
fi

echo "Starting run.sh in the background with screen. Session name: bitninja-auditd"
echo "To watch the script logs execut: [screen -r bitninja_auditd]";

# Hotfx the memory issue in the script. Need refactor in prod!
screen -S bitninja_auditd -d -m sh -c 'while true; do ./run.php; done'

